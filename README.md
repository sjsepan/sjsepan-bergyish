# Bergyish Theme

Bergyish color theme for Code-OSS based apps (VSCode, Codium, code.dev, AzureDataStudio, TheiaIDE and Positron).

Created by sjsepan.

**Enjoy!**

VSCode:
![./images/sjsepan-bergyish_code.png](./images/sjsepan-bergyish_code.png?raw=true "VSCode")
Codium:
![./images/sjsepan-bergyish_codium.png](./images/sjsepan-bergyish_codium.png?raw=true "Codium")
Code.Dev:
![./images/sjsepan-bergyish_codedev.png](./images/sjsepan-bergyish_codedev.png?raw=true "Code.Dev")
Azure Data Studio:
![./images/sjsepan-bergyish_ads.png](./images/sjsepan-bergyish_ads.png?raw=true "Azure Data Studio")
TheiaIDE:
![./images/sjsepan-bergyish_theia.png](./images/sjsepan-bergyish_theia.png?raw=true "TheiaIDE")
Positron:
![./images/sjsepan-bergyish_positron.png](./images/sjsepan-bergyish_positron.png?raw=true "Positron")

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/14/2025
