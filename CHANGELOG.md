# Bergyish Color Theme - Change Log

## [0.1.3]

- update screenshot / readme to indicate add'l supported apps
- canonical layout reorg and fix image links

## [0.1.2]

- update readme and screenshot

## [0.1.1]

- fix manifest and pub WF

## [0.1.0]

- fix manifest repo links

## [0.0.2]

- swap button.FG/BG

## [0.0.1]

- Initial release
